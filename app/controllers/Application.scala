package controllers

import play.api.mvc._
import scala.sys.process._

object Application extends Controller {

  def gitHash = "git rev-parse HEAD" !!

  def index = Action {
    Ok(views.html.main(gitHash))
  }
}