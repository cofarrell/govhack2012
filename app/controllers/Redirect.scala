package controllers

import play.api.mvc.{Action, Controller}
import play.api.libs.ws.WS
import java.net.URLEncoder

object Redirect extends Controller {

  private def encode(s: String) = URLEncoder.encode(s, "UTF-8")

  def get = Action {
    request =>
      val params = request.queryString
      val solrUrl = System.getProperty("solr.url", "http://202.2.94.228/solr")
      Async {
        WS.url(solrUrl + "?" + params.map {
          case (k, v) => encode(k) + "=" + encode(v.head)
        }.mkString("&")).get().map {
          resp => Status(resp.status)(resp.body).as("text/json")
        }
      }
  }
}
