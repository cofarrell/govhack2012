package controllers

import scala.sys.process._

import play.api.Play
import play.api.Play.current
import play.api.mvc._
import play.api.libs.concurrent.Promise
import play.api.libs.ws.WS
import play.api.libs.json._

import scalax.file.Path
import scalax.io.Resource
import net.coobird.thumbnailator.Thumbnails
import net.coobird.thumbnailator.geometry.Positions

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

object Images extends Controller {
  val sizes = Seq(150, 400)

  def get(barcode: String, size: Option[Int] = None) = Action {
    Async {
      val expires = new DateTime().plusDays(1).toString(DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss z"))
      cacheImage(barcode.replace(".jpg", ""), size).map(Ok.sendFile(_, true).withHeaders(("Expires", expires), ("Cache-Control", "public")))
    }
  }

  def getResized(barcode: String, size: Int) = get(barcode.replace(".jpg", ""), Some(size))

  val srgbMatcher = """s?rgb\((\d+),(\d+),(\d+)\)""".r

  def average(barcodes: String) = Action {
    val codes = barcodes.split(',')
    Async {
      val asyncColours = Promise.sequence(codes.map(code =>
        cacheImage(code, None).map { file =>
          try {
            var srgbMatcher(r, g, b) = Seq(
              "convert", file.getPath(),
              "-scale", "1x1!",
              "-format", "1x1!",
              "-format", "%[pixel:s]",
              "info:-"
            ).!!.trim
            Seq(code -> rgbToHsl(r.toInt, g.toInt, b.toInt))
          } catch {
            case _ => Seq()
          }
        })
      )
      asyncColours.map(coloursList => {
        Ok(JsObject(coloursList.flatten.map {
            case (k, v) => k -> JsObject(Seq(
              "h" -> JsNumber((v._1 * 360).round),
              "s" -> JsNumber(Seq(100, (v._2 * 100).round * 10).min),
              "l" -> JsNumber(50) //(v._3 * 100).round
            ))
          })
        )
      })
    }
  }

  // http://henkelmann.eu/2010/11/03/hsv_to_rgb_in_scala

  def rgbToHsl(or: Int, og: Int, ob: Int): (Double, Double, Double) = {
    val sr = or / 255.0
    val sg = og / 255.0
    val sb = ob / 255.0
 
    val max = Seq(sr, sg, sb).max
    val min = Seq(sr, sg, sb).min
    val l = (max + min) / 2

    if(max == min)
      (0, 0, l)
    else {
      val d: Double = max - min
      val s = if(l > 0.5) d / (2 - max - min) else d / (max + min)
      val h = max match {
        case `sr` => (sg - sb) / d + (if(sg < sb) 6 else 0)
        case `sg` => (sb - sr) / d + 2
        case `sb` => (sr - sg) / d + 4
      }
      (h / 6, s, l)
    }
  }

  private def cacheImage(barcode: String, size: Option[Int]) = {
    val file = imageFile(barcode, size)
    if(!file.exists) {
      val asyncResponse = requestFromNAA(barcode)
      asyncResponse.map(response => {
        cacheImageFromStream(barcode, file, response.ahcResponse.getResponseBodyAsStream())
        file
      })
    } else {
      Promise.pure(file)
    }
  }

  private def requestFromNAA(barcode: String) =
    WS.url("http://recordsearch.naa.gov.au/NAAMedia/ShowImage.asp?B=%s&T=P&S=1" format barcode).get

  private def cacheImageFromStream(barcode: String, inFile: java.io.File, in: java.io.InputStream) {
    Resource.fromInputStream(in).copyDataTo(Path(inFile))
    sizes.map(size => {
      val outFile = imageFile(barcode, Some(size))
      Thumbnails.of(inFile).size(size, size).crop(Positions.CENTER).toFile(outFile)
    })
  }

  private def imageFile(barcode: String, size: Option[Int]) = {
    Play.application.getFile(imageLocation(barcode, size))
  }

  private def imageLocation(barcode: String, size: Option[Int]) = {
    val filename = size.map("%s-%d".format(barcode, _)).getOrElse("%s".format(barcode))
    "data/images/%s.jpg".format(filename)
  }
}
