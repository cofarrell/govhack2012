The National Archives of Australia has amazing collection our countries vibrant history, with over 300,000 images collected from the past 200 years. Sadly the ability to [search](http://recordsearch.naa.gov.au/scripts/PhotoSearchSearch.asp) these images on the official site is a little cumbersome, not to mention slow.

[
![](http://i.imgur.com/eUxr8l.jpg)
](http://i.imgur.com/eUxr8.jpg)

You should be able to be bask in the ocean of images this archive has locked away. Scroll past hundreds of images, without ever having to click 'next'. Dynamically filter results on the fly by state and year with an intuitive interface, with updates the search results immediately. Tweet gems to your friends and followers. Access the images from your mobile - anywhere, anytime.

And now you can! We present to you: [PhotoSearch](http://photosearch.us.to/)

[
![](http://i.imgur.com/Phu9bl.jpg)
](http://i.imgur.com/Phu9b.jpg)

These images are more than just pretty pictures; they reveal to us our history and identity; where we come from and what makes us Australian. Discover this country through its' history and through these awesome archive of images.

Some nifty features we managed to squeeze in over the weekend:

* Fast searching!
* Magical continuous scrolling for more results
* Background colour and image borders taken from the average of the colours of search results
* Share awesome photos with your friends via twitter or [url](http://photosearch.us.to/?q=dogs&i=11674410)
* Filter and search results easily by date, state
* Mobile friendly responsive design

A few more screenshots of [PhotoSearch](http://photosearch.us.to/).

[
![](http://i.imgur.com/jtHGml.jpg)
](http://i.imgur.com/jtHGm.jpg)

[
![](http://i.imgur.com/lCPkil.jpg)
](http://i.imgur.com/lCPki.jpg)

[
![](http://i.imgur.com/gf1FDl.png)
](http://i.imgur.com/gf1FD.png)

![](https://img.skitch.com/20120603-1fi5ehbqnt3cb37riadnr17sc4.png)

That link again, just in case you missed it: [PhotoSearch](http://photosearch.us.to/).
