(function( $ ){

	$.fn.slider = function(min, max) {
		var self = this;
		// Find the anchors below this
		var anchors = this.find('.ui-slider-handle');
		var $range = this.find('.ui-slider-range');
		var $left = $(anchors[0]);
		var $right = $(anchors[1]);
		var leftMargin = 20; // Yuck - fix me

		var updateSlider = function() {
			$range.css('left', $left.css('left'));
			$range.css('width', $right.offset().left - $left.offset().left);
		}
		var calcLeft = function(v) {
			return (v - min) / (max - min) * self.width();
		}
		var calcYear = function(x) {
			return Math.round(x / self.width() * (max - min) + min);
		}

		var bind = function($a) {
			$a.bind('drag', function(e) {
				var year = calcYear(e.pageX - self.offset().left);
				year = Math.min(Math.max(year, min), max);
				$(this).css({left: calcLeft(year)});
				$a.text(year);
				updateSlider();
				self.trigger('change');
			});
			$a.bind('dragend', function(e) {
				self.trigger('done');
			});
		};
		setTimeout(function() {
			bind($left);
			bind($right);
		});

		var setX = function($e, v) {
			$e.text(v);
			$e.css({left: calcLeft(v)});
			updateSlider();
		}

		var getX = function($e) {
			if ($e.offset().left == 0) {
				return null;
			}
			return calcYear(($e.offset().left + leftMargin) - self.offset().left);
		}

		this.setLower = function(v) {
			setX($left, v || min);
		}

		this.setUpper = function(v) {
			setX($right, v || max);
		}

		this.getLower = function() {
			return getX($left) || min
		}

		this.getUpper = function() {
			return getX($right) || max;
		}

		this.hasUpper = function() {
			return self.getUpper() < max;
		}

		this.hasLower = function() {
			return self.getLower() > min;
		}

		return this;
	};
})( jQuery );