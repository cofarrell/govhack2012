$(function() {
	$results = $("#results");
	$slider = $("#date-range");
	$slider.slider(1800, new Date().getFullYear());

        // Browsers suck and sometimes return back an empty document.title
        // Crazy
        var realDocumentTitle = document.title;
        function updateDocumentTitle(s) {
                realDocumentTitle = s;
                document.title = s;
        }

	ps = {};

	ps.lastSearch = getQueryParams().q;
	ps.sort = getQueryParams().o;
	ps.image = getQueryParams().i;
	ps.count = 0;
	ps.rows = 100;
	ps.lastCategory = "";

	ps.resultsTemplate= _.template($("#image-template").html());
	ps.noMoreTemplate= _.template($("#nomore-template").html());
	ps.zeroTemplate= _.template($("#zero-template").html());

	Shadowbox.init({
		skipSetup: true
	});

 	ps.displayImageFromURL = function() {
		if (ps.image) {
			// Do a search for that image
			var q = "+barcode_no:" + ps.image
			var url = "/solr?q=" + encodeURIComponent(q) + "&version=2.2&start=0&rows=1&indent=on&wt=json";
			$.ajax({
				url:url,
				success:function (data) {
					sanitiseTitle(data.response.docs);
					var element = $(ps.resultsTemplate({doc: data.response.docs[0]}));
					element.hide();
					$("#footer").append(element);
					setTimeout(function() {
		                                var originalTitle = realDocumentTitle;
						Shadowbox.clearCache();
						Shadowbox.setup("#footer a", {
							onOpen: function(e){
				                                updateDocumentTitle(e.title);
								ps.image = e.content.split("/")[4];
								ps.updateURL();
								attachTwitterButton();
								return true;
							},

							onClose: function() {
								// TODO clean up lazy copy paste job
								Shadowbox.clearCache();
								Shadowbox.setup("#results a", {
									continuous: true,
									overlayOpacity: 0.7,
									resizeDuration: 0.6,
									onOpen: function(e){
				                                                updateDocumentTitle(e.title);
										ps.image = e.content.split("/")[4];
										ps.updateURL();
										attachTwitterButton();
										return true;
									},
									onChange: function(e) {
				                                                updateDocumentTitle(e.title);
										ps.image = e.content.split("/")[4];
										ps.updateURL();
										return true;
									},
									onClose: function(e) {
				                                                updateDocumentTitle(originalTitle);
										ps.image = undefined;
										ps.updateURL();
									}
								});
							}
						});

						var evt = document.createEvent("HTMLEvents");
						evt.initEvent("click", true, true)
						evt.preventDefault();
						element[0].dispatchEvent(evt);
					}, 3000)
				}
			});
		}
	}();

        var twitterButtonLink = '<a href="https://twitter.com/share" class="twitter-share-button" data-hashtags="govhack" data-dnt="true">Tweet</a>';
	var twitterButtonScript = '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
	var attachTwitterButton = function(){
                if(!$('#sb-info-inner .twitter').length) {
                        $('#sb-info-inner').prepend('<div class="twitter"></div>');
                }
                var button = $(twitterButtonLink).attr('data-text', realDocumentTitle).attr('data-url', document.location.toString());
                $('#sb-info-inner .twitter').html(button);

                $('#twitter-wjs').remove();
                $(document.body).append(twitterButtonScript);
	};

	ps.display = function(data) {
		var response = data.response;
		// Sanitize the title
		var shortTitles = sanitiseTitle(response.docs);
		if (response.start === 0) $results.empty();
		_.each(response.docs, function(doc) {
			var category = doc[ps.sort];
			if (ps.lastCategory != category) {
				$results.append($("<span />").addClass("category").text(category))
			}
			$results.append(ps.resultsTemplate({doc: doc}));
			ps.lastCategory = category;
		});

        createImagesBoxShadowsFromCache();

		ps.noMoreResults = response.docs.length + response.start == response.numFound && !_.isEmpty(response.docs);
		if(ps.noMoreResults) {
			$results.append(ps.noMoreTemplate());
		}

		if(response.docs.length === 0) {
			$results.append(ps.zeroTemplate());
		}

		var originalTitle = realDocumentTitle;
		Shadowbox.clearCache();
		Shadowbox.setup("#results a", {
			continuous: true,
			overlayOpacity: 0.7,
			resizeDuration: 0.6,
			onOpen: function(e){
				return this.onChange(e);
			},
			onChange: function(e) {
				updateDocumentTitle(e.title);
				ps.image = e.content.split("/")[4];
				ps.updateURL();
				attachTwitterButton();
				return true;
			},
			onClose: function(e) {
				updateDocumentTitle(originalTitle);
				ps.image = undefined;
				ps.updateURL();
			}
		});
		
	}

	ps.getLowerDate = $slider.getLower;
	ps.getUpperDate = $slider.getUpper;

	$slider.setLower(getQueryParams().l);
	$slider.setUpper(getQueryParams().u);

	ps.updateURL = function() {
		var input = [];
		var locations = [];
		_(ps.aus).each(function(enabled, state) {
			if(enabled) {
				locations.push(state);
			}
		});
		
		if (ps.lastSearch) {
			input.push("q=" + ps.lastSearch);
		}
		if (locations.length > 0 && locations.length < Object.keys(ps.aus).length) {
			input.push("s=" + locations.join(","));
		}
		if ($slider.hasLower()) {
			input.push("l=" + ps.getLowerDate());
		}
		if ($slider.hasUpper()) {
			input.push("u=" + ps.getUpperDate());
		}
		if (ps.sort) {
			input.push("o=" + ps.sort);
		}
		if (ps.image) {
			input.push("i=" + ps.image);
		}
		History.replaceState({search: input}, null, "?" + input.join("&"))
	}

	ps.newSearch = function() {
		ps.updateURL();
		ps.noMoreResults = false;
		ps.count = 0;
		ps.lastCategory = "";
		ps.search(ps.lastSearch);
                imageAverageCache = {};
	}

	ps.search = function (query) {
		var deferred = $.Deferred();
		if (_.isEmpty(query) || ps.noMoreResults) return;
		var q = [];
		q.push("+title:(" + query.replace('(','\\(').replace(')','\\)') + ")");
		q.push("+start_date:[" + ps.getLowerDate()  + " TO " + ps.getUpperDate() + "]");
		
		var locations = [];
		_(ps.aus).each(function(enabled, state) {
			if(enabled) {
				locations.push("location_within_archives:" + state.toUpperCase());
			}
		});
		
		if (locations.length > 0 && locations.length < Object.keys(ps.aus).length) {
			q.push("+(" + locations.join(" ") + ")");
		}
		
		var url = "/solr?q=" + encodeURIComponent(q.join(" ")) + "&version=2.2&start=" + ps.count + "&rows=" + ps.rows + "&indent=on&wt=json";
		
		if (ps.sort) {
			url += "&sort=" + ps.sort + "%20desc";
		}
		if (!_.isUndefined(ps.searchInProgress)) {
			ps.searchInProgress.abort();
		}
		
		ps.searchInProgress = $.ajax({
			url:url,
			success:function (data) {
                var barcodes = _.map(data.response.docs, function(v) {
                    return v.barcode_no;
                });;
				
				ps.count += data.response.docs.length;
				ps.display(data)
				$('#resultStats').text("About " + data.response.numFound + " results (" + (data.responseHeader.QTime / 1000) + " seconds)")

				ps.searchInProgress = undefined;

				deferred.resolve();				
			}
		});
		
		return deferred;
	}

	$("#search").keyup(function(e) {
		var input = $("#search").val();
		if (input === ps.lastSearch) {
			return;
		}
		ps.lastSearch = input;
		// Fire off search request here
		ps.newSearch();
	});

	$("#search").val(ps.lastSearch);
	ps.newSearch();

        function isScrolledIntoView(elem) {
                var docViewTop = $(window).scrollTop();
                var docViewBottom = docViewTop + $(window).height();

                var elemTop = $(elem).offset().top;
                var elemBottom = elemTop + $(elem).height();

                return (elemBottom <= docViewBottom) && (elemTop >= docViewTop);
        }

        var imageAverageCache = {};
        function createImagesBoxShadowsFromCache() {
            _.each(imageAverageCache, function(v, k) {
                    $('*[data-id="' + k + '"]').css('box-shadow', '0 0 20px hsl(' + [v.h, v.s + '%', v.l + '%'].join(', ') + ')');
            });
        }

        function updateBackground(barcodes) {
                var data = _.pick(imageAverageCache, barcodes);
                var dataLength = _.keys(data).length;
                var h = Math.round(_.reduce(data, function(a, v) {
                        return a + v.h;
                }, 0) / dataLength);
                var s = Math.round(_.reduce(data, function(a, v) {
                        return a + v.s;
                }, 0) / dataLength) + '%';
                var l = Math.round(_.reduce(data, function(a, v) {
                        return a + v.l;
                }, 0) / dataLength) + '%';

                $("#container").stop().animate({
                        backgroundColor: 'hsla(' + [h, s, l].join(', ') + ', 0.4)'
                }, 500);
        }

        var updateImageAverageCache = _.debounce(function(barcodesToFetch, barcodes) {
                $.ajax({
                        url: '/average/' + barcodesToFetch.join(','),
                        success: function(data) {
                                imageAverageCache = _.extend(imageAverageCache, data);
                                createImagesBoxShadowsFromCache();
                                updateBackground(barcodes);
                        }
                });
        }, 900);

        var throttledBackgroundUpdate = _.debounce(function() {
                var barcodes = $('*[data-id]').filter(function() {
                        return isScrolledIntoView(this);
                }).map(function() {
                        return $(this).data('id');
                }).get();

                if(!barcodes.length) return;

                var barcodesToFetch = _.difference(_.map(barcodes, function(v) {
                    return v.toString();
                }), _.keys(imageAverageCache));
                if(!barcodesToFetch.length) return updateBackground(barcodes);

                updateImageAverageCache(barcodesToFetch, barcodes);
        }, 100);

	// Magical never-ending scrolling
	var scroll = function (doc, $el, container, callback) {
		$(doc).scroll(function (e) {
			if ($el.scrollTop()  < container.scrollHeight - 2000) {
				return;
			}
			callback();
		});
	}
	// Stop scroll logic will in motion
	var block = function (callback) {
		var loading = false;
		return function () {
			if (loading) return;
			loading = true;
                        var response = callback();
                        if(!response) {
                            loading = false;
                            return;
                        }
			response.done(function () {
				loading = false;
			});
		}
	}
	scroll(document, $(document), $('#container')[0], block(function () {
		return ps.search(ps.lastSearch);
	}));
	
	$slider.bind('done', function(e) {
		ps.newSearch();
	});

	function updateSortDropdown() {
		// Pretty name:
		var sortOrder = $("li[data-sortorder="+ps.sort+"] a").text()
		sortOrder = sortOrder === "" ? "Sort By" : sortOrder;
		$('#sort a.dropdown-toggle').html(sortOrder + " <span class='caret'></span>");	
	}
	
	updateSortDropdown();
	$('#sort li').click(function(e) {
		ps.sort = $(this).data("sortorder");
		updateSortDropdown();
		ps.newSearch();
	})

        $(document).scroll(function() {
                throttledBackgroundUpdate();
        });

	// Hack the title so it is sane
	function sanitiseTitle(docs) {
		var titleRegex = /^TITLE: (.+?) CATEGORY.*/;
		_.each(docs, function(imageData){
			var title = shortTitle = imageData.title;
			var match = titleRegex.exec(title);
			if (match !== null) {
				shortTitle = match[1];
			}
			// Roughly limit title to a sane length
			var SANE_LENGTH = 15;
			var titleSplit = shortTitle.split(" ");
			if (titleSplit.length > SANE_LENGTH) {
				shortTitle = titleSplit.splice(0, SANE_LENGTH).join(" ");
				shortTitle += "...";
			}
			imageData.shortTitle = shortTitle;
		});
	}
});

function getQueryParams() {
	return (function(a) {
		if (_.isEqual(a, [""])) return {q: ""};
		var b = {};
		for (var i = 0; i < a.length; ++i)
		{
			var p=a[i].split('=');
			if (p.length != 2) continue;
			b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	})(window.location.search.substr(1).split('&'));
}

// g+ +1 icon
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();

// facebook like
  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
