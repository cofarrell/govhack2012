import sbt._
import Keys._
import PlayProject._

object ApplicationBuild extends Build {

  val appName = "helloworld"
  val appVersion = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    "net.databinder" %% "dispatch-http" % "0.8.8",
    "org.scalaz" %% "scalaz-core" % "7.0-SNAPSHOT",
    "net.databinder" %% "dispatch-json" % "0.8.8",
    "net.liftweb" %% "lift-json" % "2.4",
    "net.coobird" % "thumbnailator" % "0.4.2"
  )

  val main = PlayProject(appName, appVersion, appDependencies, mainLang = SCALA).settings(
    // Add your own project settings here
  )

}
