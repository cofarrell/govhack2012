#!/usr/bin/env python

import json
import urllib2
from collections import defaultdict

def hit_to_cache_image(barcode):
    print "Hitting %s" % barcode
    url = urllib2.urlopen('http://202.2.94.228/image/%s/150' % barcode)
    url.close()

barcodes_by_year = defaultdict(list)

for year in range(1900, 2012):
    url = urllib2.urlopen('http://202.2.94.228/solr?q=start_date:%d&rows=100&indent=on&wt=json' % year)
    data = json.loads(url.read())
    url.close()

    for doc in data['response']['docs']:
        barcode = doc['barcode_no']
        hit_to_cache_image(barcode)
        barcodes_by_year[year].append(barcode)

f = open('first_100_from_years.json', 'w')
f.write(json.dumps(barcodes_by_year))
f.close()
